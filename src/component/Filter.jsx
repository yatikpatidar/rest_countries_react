import React, { useContext } from 'react'
import ThemeContext from '../context/ThemeContext';


const Filter = ({ target, onChange, filterBy, selectedValue }) => {
    const themeObj = useContext(ThemeContext);

    function handleFilter(event) {
        onChange(event);
    }
    return (
        <div  >
            <select onChange={(event) => handleFilter(event)} value={selectedValue} className={` shadow-7xl mt-4 py-3 px-2 ${themeObj['mode']}`}>
                {filterBy === 'Sub-Region' && <option value="">filter by {filterBy}</option>}

                {target.map((iterator, index) => {
                    {
                        return iterator ? (
                            <option key={`${filterBy + index}`} value={iterator} >{iterator}</option>
                        ) : (
                            <option key={`${filterBy}1`} disabled> No sub-region found !!</option>
                        )
                    }
                })}
            </select>
        </div>
    )
}

export default Filter