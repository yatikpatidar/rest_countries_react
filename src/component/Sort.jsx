import React, { useContext } from 'react'
import ThemeContext from '../context/ThemeContext'

const Sort = ({ onChange, sortBy, selectedValue }) => {

    const themeObj = useContext(ThemeContext);
    function handleFilter(value) {

        onChange(value);
    }
    return (

        <div key="populationFilter" className={themeObj['mode']}>
            <select value={selectedValue} onChange={(event) => handleFilter(event.target.value)} className={`shadow-2xl  mt-4 py-3 px-2 ${themeObj['mode']}`}>
                <option value="" hidden >Sort by {sortBy} </option>

                <option value="Ascending" >Ascending</option>
                <option value="Descending" >Descending</option>
            </select>
        </div>
    )
}

export default Sort