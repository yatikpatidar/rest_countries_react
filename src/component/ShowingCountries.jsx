import React from 'react'
import { useContext } from 'react';
import ThemeContext from '../context/ThemeContext';
import Loader from './Loader';
import CountryCard from './CountryCard';


const ShowingCountries = ({ filteredData, fetchStatus }) => {

    const themeObj = useContext(ThemeContext);

    return (
        <>
            {fetchStatus === 'loading' ? <div className='mx-[48%]  my-[15%]'><Loader /> </div> : (

                <section className={themeObj['mode']}>
                    {fetchStatus === 'error' ? (
                        <section className="text-center flex flex-col justify-center items-center h-[80vh]">
                            <p className="text-4xl mb-5">Error while fetching data </p>
                        </section>
                    ) : (
                        <>
                            {filteredData.length === 0 ? (
                                <section className="text-center flex flex-col justify-center items-center h-[80vh]">
                                    <p className="text-4xl mb-5">No such country Found </p>
                                </section>

                            ) : (

                                <div className=' mx-auto max-w-[1200px] flex flex-wrap justify-between gap-8 py-10 '>

                                    {filteredData.map((country, index) => {

                                        return <div key={`country${index}`}>

                                            <CountryCard country={country} index={index} />
                                        </div>

                                    })}
                                </div>
                            )}
                        </>
                    )}
                </section >
            )}
        </>
    )
}

export default ShowingCountries