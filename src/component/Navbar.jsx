import React, { useContext } from 'react'
import { MdDarkMode } from "react-icons/md";
import ThemeContext from '../context/ThemeContext';

const Navbar = () => {
    const themeObj = useContext(ThemeContext);

    return (
        <>
            <section>

                <nav className={` border-b-2 ${themeObj['mode']}`}>
                    <div className=' mx-auto max-w-[1200px] pt-4  '>
                        <div className='flex justify-between '>
                            <div>Where in the world ?</div>

                            <div className='flex '>
                                <MdDarkMode className='m-3 mr-1 ' />
                                <button onClick={() => { themeObj.setMode(themeObj['mode'] === '' ? "bg-slate-800 text-white" : '') }}> Mode</button>
                            </div>
                        </div>
                    </div>
                </nav>

            </section>
        </>
    )
}

export default Navbar