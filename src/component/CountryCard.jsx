import React from 'react'
import { Link } from 'react-router-dom';


const CountryCard = ({ country, index }) => {

    return (
        <Link key={index} to={`country/${country["ccn3"]}`}>
            <div className=" w-[265px] h-[420px] shadow-2xl rounded-xl p-2 " id='card-container'>

                <img className='max-w-[250px] h-[180px]' src={country['flags']['png']} alt={country['name']['common']} />
                <h2 className="text-xl font-bold px-4 py-6 max-w-[250px]">{country['name']['common']}</h2>

                <p className='px-4 pb-2'>
                    <b>population:</b> {country['population']}
                </p>

                <p className='px-4 pb-2'>
                    <b>Region:</b> {country['region']}
                </p>

                <p className='px-4 pb-2'>
                    <b>capital:</b> {country['capital']}
                </p>
            </div>
        </Link>
    )
}

export default CountryCard