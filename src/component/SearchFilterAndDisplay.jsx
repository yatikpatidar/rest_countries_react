import React from 'react'
import { useState, useContext } from 'react';
import ThemeContext from '../context/ThemeContext';
import { FaSearchLocation } from "react-icons/fa";
import ShowingCountries from './ShowingCountries';
import Filter from './Filter';
import Sort from './Sort';


const SearchFilterAndDisplay = ({ countriesData, fetchStatus }) => {

    const themeObj = useContext(ThemeContext);

    const [filterQuery, setFilterQuery] = useState({
        inputCountry: "",
        selectedRegion: "All",
        selectedSubRegion: "",
        sortByPopulation: "",
        sortByArea: ""
    })

    const regionData = countriesData.reduce((regionDataObj, country) => {
        const region = country['region'];
        const subRegion = country['subregion'];

        if (regionDataObj[region] === undefined) {
            regionDataObj[region] = [subRegion]

        } else {
            if (!regionDataObj[region].includes(subRegion)) {
                regionDataObj[region].push(subRegion);
            }
        }
        return regionDataObj;

    }, { 'All': [] })

    function handleCountrySearch(event) {

        const input = event.target.value;
        setFilterQuery({
            ...filterQuery, inputCountry: input
        })

    }

    function handleSelectedRegion(event) {

        setFilterQuery({
            ...filterQuery,
            selectedRegion: event.target.value,
            selectedSubRegion: ""
        })
    }

    function handleSelectedSubRegion(event) {

        setFilterQuery({
            ...filterQuery,
            selectedSubRegion: event.target.value
        })
    }

    function handleSortByPopulationFun(value) {

        setFilterQuery({
            ...filterQuery,
            sortByPopulation: value,
            sortByArea: ""
        })
    }

    function handleSortByAreaFun(value) {

        setFilterQuery({
            ...filterQuery,
            sortByArea: value,
            sortByPopulation: ""
        })

    }

    function filterCountriesFun() {

        let filteredCountries = countriesData;

        if (filterQuery['inputCountry']) {
            filteredCountries = filteredCountries.filter((country) => {
                const countryName = country['name']['common'].toLowerCase();
                if (countryName.includes(filterQuery['inputCountry'].toLowerCase())) {
                    return true
                }
            })
        }

        if (filterQuery['selectedRegion'] !== 'All') {
            filteredCountries = filteredCountries.filter((country) => country['region'] === filterQuery['selectedRegion'])
        }

        if (filterQuery['selectedSubRegion']) {
            filteredCountries = filteredCountries.filter((country) => country['subregion'] === filterQuery['selectedSubRegion'])
        }

        if (filterQuery['sortByPopulation']) {
            if (filterQuery['sortByPopulation'] === "Descending") {
                filteredCountries.sort((country1, country2) => country2['population'] - country1['population'])
            } else {
                filteredCountries.sort((country1, country2) => country1['population'] - country2['population'])
            }

        } else if (filterQuery['sortByArea']) {
            if (filterQuery['sortByArea']) {
                if (filterQuery['sortByArea'] === "Descending") {
                    filteredCountries.sort((country1, country2) => country2['area'] - country1['area']);
                } else {
                    filteredCountries.sort((country1, country2) => country1['area'] - country2['area']);
                }
            }
        }

        return filteredCountries
    }

    const filteredData = filterCountriesFun()


    return (
        <>
            <section className={themeObj['mode']}>

                <div >
                    {fetchStatus === 'loading' ? <div></div> : (
                        <div className='flex justify-between mx-auto max-w-[1200px] '>

                            <div className='flex  shadow-2xl rounded text-[18px] mt-2 py-2 px-2 '>
                                <FaSearchLocation className='m-3 mx-5' />
                                <input onChange={(event) => handleCountrySearch(event)} type="text" name='country' id='searchCountry' className={` pl-3 ${themeObj['mode']}`} placeholder=' Search for a Country... ' />
                            </div>

                            <Sort onChange={handleSortByPopulationFun} sortBy={"Population"} selectedValue={filterQuery['sortByPopulation']} />

                            <Sort onChange={handleSortByAreaFun} sortBy={"Area"} selectedValue={filterQuery['sortByArea']} />

                            <Filter target={Object.keys(regionData)} onChange={handleSelectedRegion} filterBy={"Region"} selectedValue={filterQuery['selectedRegion']} />

                            {filterQuery['selectedRegion'] === 'All' ? (<div></div>) : (

                                <div className={themeObj['mode']}>
                                    <Filter target={regionData[filterQuery['selectedRegion']]} onChange={handleSelectedSubRegion} filterBy={"Sub-Region"} selectedValue={filterQuery['selectedSubRegion']} />

                                </div>
                            )}
                        </div>
                    )}
                </div>
            </section>

            <ShowingCountries filteredData={filteredData} fetchStatus={fetchStatus} />
        </>
    )
}

export default SearchFilterAndDisplay