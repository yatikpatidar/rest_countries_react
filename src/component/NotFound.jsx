import { React, useContext } from 'react'
import ThemeContext from '../context/ThemeContext';


const NotFound = () => {
    const themeObj = useContext(ThemeContext);
    return (
        <section className={` ${themeObj['mode']} text-center flex flex-col justify-center items-center  h-[95vh]`}>

            <h1 className="text-6xl font-bold mb-4">404 Not Found</h1>
            <p className="text-xl mb-5">This page does not exist</p>

        </section>
    )
}

export default NotFound