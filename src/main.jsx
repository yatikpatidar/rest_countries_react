import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { Route, RouterProvider, createBrowserRouter, createRoutesFromElements } from 'react-router-dom'
import Layout from './Layout.jsx'
import NotFound from './component/NotFound.jsx'
import HomePage from './pages/HomePage.jsx'
import CountryDetail from './pages/CountryDetail.jsx'


const router = createBrowserRouter(
  createRoutesFromElements(

    <Route path='/' element={<Layout />}>

      <Route path='' element={<HomePage />} />
      <Route path='country/:id' element={<CountryDetail />} />
      <Route path='*' element={<NotFound />} />

    </Route>
  )
)

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
