import React, { useState } from 'react'
import ThemeContext from './ThemeContext'

const ThemeState = (props) => {

  const [mode, setMode] = useState('');

  return (
    <>
      <ThemeContext.Provider value={{ mode, setMode }}>
        {props.children}

      </ThemeContext.Provider>

    </>
  )
}

export default ThemeState