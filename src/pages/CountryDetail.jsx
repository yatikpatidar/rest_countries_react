import React, { useState, useEffect, useContext } from 'react'
import { useParams } from 'react-router-dom'
import { Link } from 'react-router-dom';
import ThemeContext from '../context/ThemeContext';
import Loader from '../component/Loader';
import NotFound from '../component/NotFound';


const CountryDetail = () => {

    let { id } = useParams()

    const themeObj = useContext(ThemeContext);

    const [fetchStatus, setFetchStatus] = useState("loading");
    const [countriesData, setCountriesData] = useState([]);

    let countriesBorderObject = {}

    useEffect(() => {

        const fetchCountriesData = async () => {
            try {
                const responseData = await fetch("https://restcountries.com/v3.1/all")
                const jsonData = await responseData.json();

                setCountriesData(jsonData);
                setFetchStatus("success");

            } catch (err) {
                setFetchStatus('error');
            }
        }

        fetchCountriesData();

    }, [])

    countriesData.forEach((country, index) => {
        countriesBorderObject[country['cca3']] = index;
    })

    const countryDetail = countriesData.reduce((result, country) => {

        if (country['ccn3'] === id) {

            result['countryName'] = country['name']['common'];
            result['imgUrl'] = country["flags"]["png"]

            const nativeNameKeys = Object.keys(country['name']['nativeName']);
            result['nativeName'] = country['name']['nativeName'][nativeNameKeys[0]]['common'];

            result['topLevelDomain'] = country['tld'][0];

            result['population'] = country['population'];

            const currenciesKeys = Object.keys(country['currencies']);
            result['currencies'] = country['currencies'][currenciesKeys[0]]['name'];

            result['region'] = country['region'];

            const languagesKeys = Object.keys(country['languages']);
            result['languages'] = ''


            for (let key of languagesKeys) {
                result['languages'] += country['languages'][key] + " ,";
            }

            result['subRegion'] = country['subregion'];

            result['captial'] = country['capital'];


            result['borders'] = country['borders'];

        }
        return result

    }, {})


    return (
        <>
            {fetchStatus === 'loading' ? (<div className='mx-[48%] my-[18%]'><Loader /> </div>) : (

                Object.keys(countryDetail).length === 0 ? (<div>< NotFound /></div>) : (
                    <section className={`${themeObj['mode']} h-[100vh]`} >

                        <div className='mx-auto max-w-[1200px] pt-8'>

                            <div> <Link to={`/`} >  <div className='my-2  py-2 px-6 border-2 inline '>  Back </div></Link>

                                <div className="mt-8 flex flex-wrap gap-8 shadow-2xl rounded-xl">

                                    <div>
                                        <img src={countryDetail['imgUrl']} alt={countryDetail['countryName']} className='w-[480px] h-[320px] mr-10' />
                                    </div>

                                    <div id="countryDetail" className="w-[50%]">

                                        <h2 className="text-[25px] font-medium my-2">{countryDetail['countryName']} </h2>

                                        <div className="flex flex-wrap ">

                                            <div className="w-[50%] my-1"> <b> Native Name : </b>{countryDetail['nativeName']}</div>
                                            <div className="w-[50%] my-1"> <b> Top Level Domain : </b> {countryDetail['topLevelDomain']}</div>
                                        </div>

                                        <div className="flex flex-wrap ">
                                            <div className="w-[50%] my-1"> <b> Population : </b> {countryDetail['population']} </div>
                                            <div className="w-[50%] my-1"> <b> Currencies : </b> {countryDetail['currencies']}</div>
                                        </div>

                                        <div className="flex flex-wrap ">
                                            <div className="w-[50%] my-1"> <b> Region : </b> {countryDetail['region']} </div>
                                            <div className="w-[50%] my-1"> <b> Languages : </b> {countryDetail['languages']} </div>
                                        </div>

                                        <div className="my-1.5"> <b>Sub Region : </b> {countryDetail['subRegion']} </div>
                                        <div className="my-1.5"> <b> Captial : </b> {countryDetail['captial']} </div>

                                        <div className="my-6 flex flex-wrap"> <b>Border Countries : </b>

                                            {countryDetail['borders'] ?
                                                (countryDetail['borders'].map((borderCode, index) => {

                                                    return <Link key={`border${index}`} to={`/country/${countriesData[countriesBorderObject[borderCode]]["ccn3"]}`}>
                                                        <div className='mt-8 m-4 border-2 inline '>{countriesData[countriesBorderObject[borderCode]]['name']['common']}</div>
                                                    </Link>

                                                })) : (<div className='mt-6 m-4 border-2 inline '> No Border </div>)}
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                )
            )}
        </>
    )
}

export default CountryDetail