import React, { useState, useEffect } from 'react'
import SearchFilterAndDisplay from '../component/SearchFilterAndDisplay';

const Home = () => {

    const [countriesData, setCountriesData] = useState([]);
    const [fetchStatus, setFetchStatus] = useState("loading");

    useEffect(() => {
        const fetchData = async () => {
            try {

                const res = await fetch(`https://restcountries.com/v3.1/all`)
                const jsonData = await res.json();

                setCountriesData(jsonData);
                setFetchStatus("success")

            } catch (err) {
                setFetchStatus('error')
            }
        }

        fetchData();

    }, []);

    return (
        <>
            <SearchFilterAndDisplay countriesData={countriesData} fetchStatus={fetchStatus} />
        </>
    )
}

export default Home