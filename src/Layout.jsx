import React from 'react'
import Navbar from './component/Navbar'
import ThemeState from './context/ThemeState'
import { Outlet } from 'react-router-dom'

const Layout = () => {
  return (
    <>
      <ThemeState>

        <Navbar />
        <Outlet />

      </ThemeState>
    </>
  )
}

export default Layout